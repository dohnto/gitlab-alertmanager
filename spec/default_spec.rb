require "spec_helper"

describe "gitlab-alertmanager::default" do
  include ChefVault::TestFixtures.rspec_shared_context

  context "default execution" do
    before do
      stub_search(:node, "recipes:gitlab-alertmanager\\:\\:default") {
        [{ "fqdn" => "stubbed_node",
           "hostname" => "my.hostname",
           "ipaddress" => "10.11.12.13" }]
      }
    end

    cached(:chef_run) do
      ChefSpec::SoloRunner.new { |node|
        node.normal["prometheus-alertmanager"]["alertmanager"]["chef_vault"] = "alertmanager"
      }.converge(described_recipe)
    end

    before do
      allow(Chef::DataBag).to receive(:load).with("prometheus-alertmanager").and_return("_default_keys" => { "id" => "_default_keys" })
      allow(ChefVault::Item).to receive(:load).with("alertmanager", "_default").and_return("id" => "_default",
                                                                                           "prometheus-alertmanager" => {
                                                                                             "alertmanager" => {
                                                                                               "slack" => {
                                                                                                 "api_url" => "slack_api",
                                                                                                 "channel" => "#prometheus-alerts",
                                                                                               },
                                                                                               "pagerduty" => {
                                                                                                 "service_key" => "servicekey",
                                                                                               },
                                                                                             },
                                                                                           })
    end

    it "creates a prometheus user and group" do
      expect(chef_run).to create_user("prometheus").with(
        system: true,
        shell: "/bin/false"
      )
    end

    it "creates the alertmanager dir in the configured location" do
      expect(chef_run).to create_directory("/opt/prometheus/alertmanager").with(
        owner: "prometheus",
        group: "prometheus",
        mode: "0755",
        recursive: true
      )
    end

    it "creates the log dir in the configured location" do
      expect(chef_run).to create_directory("/var/log/prometheus/alertmanager").with(
        owner: "prometheus",
        group: "prometheus",
        mode: "0755",
        recursive: true
      )
    end

    it "includes runit::default" do
      expect(chef_run).to include_recipe("runit::default")
    end

    it "creates the configuration file with default content" do
      expect(chef_run).to create_template("/opt/prometheus/alertmanager/alertmanager.yml").with(
        owner: "prometheus",
        group: "prometheus",
        mode: "0644"
      )
      expect(chef_run).to render_file("/opt/prometheus/alertmanager/alertmanager.yml").with_content { |content|
        expect(content).to include('api_url: "slack_api"')
        expect(content).to include('channel: "#prometheus-alerts"')
        expect(content).to include('service_key: "servicekey"')
      }
    end

    it "uploads all the alerting templates" do
      expect(chef_run).to create_remote_directory("/opt/prometheus/alertmanager/templates")
    end
  end
end
