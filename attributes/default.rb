#
# Cookbook Name GitLab::Monitoring
# Attributes:: default
#

default["prometheus"]["user"]        = "prometheus"
default["prometheus"]["group"]       = "prometheus"

default["alertmanager"]["dir"]              = "/opt/prometheus/alertmanager"
default["alertmanager"]["templates"]["dir"] = "#{node['alertmanager']['dir']}/templates"
default["alertmanager"]["binary"]           = "#{node['alertmanager']['dir']}/alertmanager"
default["alertmanager"]["log_dir"]          = "/var/log/prometheus/alertmanager"

default["alertmanager"]["version"]          = "0.9.1"
default["alertmanager"]["checksum"]         = "407e0311689207b385fb1252f36d3c3119ae9a315e3eba205aaa69d576434ed7"
default["alertmanager"]["binary_url"]       = "https://github.com/prometheus/alertmanager/releases/download/v#{node['alertmanager']['version']}/alertmanager-#{node['alertmanager']['version']}.linux-amd64.tar.gz"

default["alertmanager"]["flags"]["config.file"]        = "#{node['alertmanager']['dir']}/alertmanager.yml"
default["alertmanager"]["flags"]["web.listen-address"] = "0.0.0.0:9093"
default["alertmanager"]["flags"]["storage.path"]       = "#{node['alertmanager']['dir']}/data"

default["alertmanager"]["peers"] = []

default["alertmanager"]["slack"]["api_url"] = nil
