# Basing on https://github.com/prometheus/alertmanager/blob/6a3dfaff45fafba1ef8553451e6b1fc0435b6523/template/default.tmpl#L15
# We will define a shared gitlab.title template that will look like this:
#  FIRING: 1 - Title of the alert itself.
#  RESOLVED - Title of the alert itself.
# 
# For this to work, all the alerts that are grouped by environment and alert name must share the description,
# else there will be no description
{{ define "gitlab.title" }}
  {{- .Status | title }}
  {{- if eq .Status "firing" }} {{ .Alerts.Firing | len }}{{ else }} {{ .Alerts.Resolved | len }}{{ end }}
  {{- printf " - " }}{{ .CommonAnnotations.title }}
{{- end }}

# The one thing we don't have here is the value, so we will need to add it as a label at the alert level.
{{ define "gitlab.text" }}
  {{- if eq .Status "firing" }}
    {{- $ca := .CommonAnnotations }}
    {{- $cl := .CommonLabels }}
    {{- $ca.description }}
    {{ if gt (len $cl) 0 -}}
      *Labels*: {{- range $cl.SortedPairs }} *{{ .Name }}*: {{ .Value }} {{ end }}
    {{- end }}
    {{ if gt (len $ca) 0 -}}
      *Annotations*: {{- range $ca.SortedPairs }}{{ template "gitlab.common.annotations" . }}{{ end }}
    {{- end }}
    {{ if gt (len .Alerts) 0 -}}
    *Services*:
    {{- range .Alerts }}
      {{ if eq .Labels.fqdn "" }}{{ .Labels.instance }}{{ else }}{{ .Labels.fqdn }}{{ end }}
      {{- range .Annotations.SortedPairs }}
        {{- if not ( eq (index $ca .Name) .Value) }} *{{ .Name }}* = {{ .Value }}{{- end }}
      {{- end }}
    {{- end }}
    {{- end }}

    *Prometheus*: {{ with index .Alerts 0 }}{{ .GeneratorURL }}{{ end }}
  {{- end }}
{{- end }}

{{ define "gitlab.runbook.link" }}
  {{- if eq .CommonAnnotations.link "" -}}
    https://dev.gitlab.org/cookbooks/runbooks/blob/master/{{ .CommonAnnotations.runbook -}}
  {{- else -}}
    {{- .CommonAnnotations.link -}}
  {{- end -}}
{{- end }}

{{ define "gitlab.common.annotations" }}
  {{ if eq .Name "title" }}{{ else if eq .Name "description"}}{{ else if eq .name "runbook"}}{{ else if eq.name "link" }}{{ else }}*{{ .Name }}*: {{ .Value }}{{ end }}
{{ end }}
